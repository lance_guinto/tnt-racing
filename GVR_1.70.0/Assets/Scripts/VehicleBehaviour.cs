﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class VehicleBehaviour : MonoBehaviour {

	private Rigidbody cycleSphere;
	private float inputX = 0.0f;
	private float inputY = 0.0f;

	public float currentSpeed = 0.0f;
	public float maxSpeed = 2.0f;
	public float acceleration = 0.5f;
//	public float maxReverseSpeed = 10f;
	public float deceleration = 2.5f;
	public float smoothStop = 2.5f;
	public float steeringAngle = 3f;
	private float velo;

	// Use this for initialization
	void Start () {		
		cycleSphere = GetComponent<Rigidbody>();

		// Enable Gyro
		Input.gyro.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		inputX = Input.GetAxis("Horizontal");
		inputY = Input.GetAxis("Vertical");

		// Rotate cycleSphere based on device acceleration X
		// So far the best, if a little jittery
		if (currentSpeed > 0)
			cycleSphere.transform.Rotate (0, Input.acceleration.x, 0);

//		// Rotate cycleSphere based on device Gyro Rotation
//		// Produces inverted values, has drift, value doesn't always return normally
//		if (currentSpeed > 0)
//			cycleSphere.transform.Rotate (0, -Input.gyro.rotationRate.z, 0);

//		// Rotate cycleSphere based on device Gyro Attitude
//		// Overly sensitive, need calibration
//		if (currentSpeed > 0)
//			cycleSphere.transform.Rotate (0, -Input.gyro.attitude.z, 0);
	}

	void FixedUpdate () {

//			// Stopping the car when no user input given
//			var velocity = cycleSphere.velocity;
//			var localVel = transform.InverseTransformDirection(velocity);
//			currentSpeed = Mathf.SmoothDamp(currentSpeed, 0f, ref velo, Time.deltaTime * smoothStop );

		// Accelerate cycleSphere to maxSpeed if currentSpeed < maxSpeedf 
		if (currentSpeed < maxSpeed)
			currentSpeed += acceleration;

		// Rotate cycleSphere if Speed > 0, keyboard commands
		if (currentSpeed > 0)
			cycleSphere.transform.Rotate (0, inputX, 0);

		// Apply currentspeed as rigidbody velocity
		cycleSphere.velocity += transform.forward * currentSpeed;
	}
}
